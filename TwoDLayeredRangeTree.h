// Copyright (c) 2022 Stefano Leucci
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __TWODLAYEREDRANGETREE_H__
#define __TWODLAYEREDRANGETREE_H__

#include <vector>
#include <iterator>
#include <algorithm>
#include <initializer_list>
#include <type_traits>
#include <bit>

namespace range_trees
{
    template <typename P> using point_iterator = std::vector<P>::const_iterator;

    // The nodes in our range tree will inherit from a template type N to allow for dependency injection
    // this is the default node type if none is specified
    template <typename P> class default_node_t
    {
        public:
            void init([[maybe_unused]] point_iterator<P> first, [[maybe_unused]] point_iterator<P> last) {}
    };

    // An implementation of a 2D Layered Range tree storing points of type P
    // If p is a point then p[0] and p[1] must return the 1st and 2nd coordinate of p
    // Coordinates must be from a totally ordered domain and need to support the < and == operators
    // P should be a lightweight class as object of type P are copied around and multiple copies of each points are stored
    // The actual binary search tree is built over the 2nd coordinate. Each node of the tree is associate to a range
    // of values of the 2nd coordinate and stores a collection containing all points having their 2nd coordinate such a range
    // These collection are sorted according to the 1st coordinate. 
    // Cross-linking is used to quickly move from one point stored in a node to the same point, or its successor, in a child node
    // It is possible to augment the range tree via custom node types via dependency injection.
    // The template parameter N specifies the node type to use. N should implement a "init" function with a signature
    // similar to default_node_t::above . This function is called once for each node of the tree at build time 
    // and its parameter are two iterators to the first and one-past last element of the collection of points of that node.
    // The search function below allows to execute a callback function on a set of nodes that contain all points in a 2D range
    // of interest. For each node the callback receives a pointer to that node and two iterators to the first and one-past the  
    // last point of interest in that node's collection. Each point in the 2D range is reported in exactly one of these callbacks.
    template <typename P, class N = default_node_t<P>> class TwoDLayeredRangeTree
    {
        private:
            // Type of the coordinates returned by P
            typedef typename std::decay<decltype(std::declval<P>()[std::declval<int>()])>::type coord_t;

            class node_t : public N
        {
            public:
                // Range of the 2nd coordinates of the points stored in the current node, endpoints included
                coord_t range_from, range_to;

                // Points stored in this node, in non-decreasing order of their 1st coordinate
                // The order is consistent among the current node and its children
                std::vector<P> points;

                // Cross-linking pointers. The i-th entry is the index of the copy of points[i] in the child node, if it exists
                // If points[i] does not belong to that children, the i-th entry is the index of the successor of points[i]
                std::vector<std::size_t> left_pointers;  //Pointers towards the left children
                std::vector<std::size_t> right_pointers; //... and towards the right children
        };

            // The actual binary search tree, stored as a vector. The root is tree[0].
            // The left (resp. right) child of the node at index i is at index 2*i+1 (resp. 2*i+2).
            std::vector<node_t> tree;

            void build(std::vector<P> &points)
            {
                if(points.size() == 0)
                    return;

                // Sort points in non-decreasing order of their 2nd coordinate
                // break ties in non-decreasing order of their 1st coordinate
                std::sort(points.begin(), points.end(), [](const P &a, const P &b) { return a[1] < b[1] || (a[1] == b[1] && a[0] < b[0]); } );

                std::size_t distinct_coordinates = 1; // Number of distinct 2nd coordinates
                for(std::size_t i =1; i<points.size(); i++)
                    distinct_coordinates += (points[i-1][1] != points[i][1]);

                // The actual tree has distinct_coordinates-1 internal nodes and distinct_coordinates leaves
                tree.resize(2*distinct_coordinates - 1);

                // Initialize the leaves
                
                //Number of leaves on the last level of the binary search tree
                std::size_t leaves_on_last_level = 2*distinct_coordinates - std::bit_ceil(distinct_coordinates);

                auto next_point = points.cbegin(); // The first point with the next 2nd coordinate to process
                for(std::size_t i=0; i<distinct_coordinates; i++)
                {
                    // Find the i-th leaf, counting from left to right. Leaves might be on multiple levels
                    const std::size_t leaf_idx = (distinct_coordinates - 1) + (distinct_coordinates - leaves_on_last_level + i)%distinct_coordinates;
                    node_t &v = tree[leaf_idx];

                    // Find the largest range [next_point, last_point) of points having the same 2nd coordinate
                    auto last_point = next_point + 1;
                    while(last_point < points.cend() && (*(last_point-1))[1] == (*last_point)[1] )
                        last_point++;

                    v.range_from = v.range_to = (*next_point)[1]; // A leaf stores a single coordinate
                    v.points.insert(v.points.begin(), next_point, last_point); // ... and all the points in the range we just found
                    v.init(v.points.cbegin(), v.points.cend());

                    next_point = last_point;
                }

                // Initialize the internal nodes, ensure that they are examined in postorder
                for(std::size_t i=0; i<distinct_coordinates-1; i++)
                {
                    const size_t v = distinct_coordinates-2-i; // Index of the current node

                    const node_t &left = tree[2*v+1];  // Left child of v
                    const node_t &right = tree[2*v+2]; // Right child of v

                    tree[v].range_from = left.range_from; // The range of v goes from the first endpoint of its left child 
                    tree[v].range_to = right.range_to; // ...to the second endpoint of its right child (inclusive)

                    // Reserve enough space to contain all the points in the left and right children and the cross-linking pointers
                    std::size_t npoints = left.points.size() + right.points.size();
                    tree[v].points.reserve(npoints);
                    tree[v].left_pointers.reserve(npoints);
                    tree[v].right_pointers.reserve(npoints);

                    // Merge "a-la Mergesort", initialize cross-linking pointers
                    std::size_t left_idx = 0, right_idx = 0;
                    for(; npoints>0; npoints--)
                    {
                        tree[v].left_pointers.push_back(left_idx);
                        tree[v].right_pointers.push_back(right_idx);

                        if(right_idx == right.points.size() || (left_idx != left.points.size() && left.points[left_idx][0] < right.points[right_idx][0]))
                            tree[v].points.push_back(left.points[left_idx++]);
                        else
                            tree[v].points.push_back(right.points[right_idx++]);
                    }

                    tree[v].init(tree[v].points.cbegin(), tree[v].points.cend());
                }
            }

            // The node v contains some points of interest in a range starting at index begin. y is the last 1st coordinate
            // of interest and end is either the last element with coordinate y, or its successor.
            // This function first ensures that end is always exactly one element past the last point of interest and then calls
            // func(&node, begin, end) if the range [begin, end) is not empty
            template <typename F> void handle_node(node_t *v, const coord_t y, std::size_t begin, std::size_t end, F func)
            {
                // Make sure that end points to the successor of y
                if(end < v->points.size() && !(y < v->points[end][0]))
                    end++;

                if(begin>=end) // If the range is empty there is nothing to do
                    return;

                func(static_cast<N*>(v), v->points.cbegin() + begin, v->points.cbegin() + end);
            }


            // Follows a cross-linking pointer ptr. Returns the updated pointer for the target node target_node
            // pointers is a vector of of pointers from the current node to target_node.
            std::size_t follow_xlinking_pointer(std::size_t ptr, const std::vector<std::size_t> &pointers, std::size_t target_node) const
            {
                if(ptr < pointers.size())
                    return pointers[ptr];
                else
                    return tree[target_node].points.size();
            }

        public:
            // Constructs a 2D layered range tree from the points in [first, last)
            template <class InputIt> TwoDLayeredRangeTree(InputIt first, InputIt last)
            {
                std::vector<P> points(first, last);
                build(points);
            }

            // Constructs a 2D layered range tree from an initializer list
            TwoDLayeredRangeTree(std::initializer_list<P> l)
            {
                std::vector<P> points(l);
                build(points);
            }

            // Locates O(log n) nodes such each point in the rectangle defined by x and y is stored in exactly one of these nodes,
            // where n is the number of points stored in the range tree
            // For each node, the points of interest are always in a contiguous range [begin, end).
            // This function calls func(v, begin, end) for each node v with a non-empty range [begin, end)
            template <typename F> void search(const P x, const P y, F func)
            {
                if(tree.size() == 0 || y[1] < tree[0].range_from || tree[0].range_to < x[1])
                    return;

                // Performs 2 searches in the BST, one for x[1] (the 2nd coordinate of x) and the other for y[1] 
                // (the 2nd coordinate of y).
                // l (resp. r) is the node followed by the first "left" (resp. second "right") search.
                std::size_t l = 0, r = 0; //Initially both l and r are just the root of the BST

                // We keep the cross-linking pointers updated to the range of 1st coordinates of interest during the traversal
                // l_begin and l_end refer to the "left" search, while r_begin and r_end refer to the "right" search
                // Initially l_begin = r_begin, l_end = r_end, and the range is that of the root
                std::size_t l_begin = std::distance(tree[0].points.begin(), std::lower_bound(tree[0].points.begin(), tree[0].points.end(), x, [](const P &a, const P &b) { return a[0] < b[0]; } ) );
                std::size_t l_end = std::distance(tree[0].points.begin(), std::upper_bound(tree[0].points.begin(), tree[0].points.end(), y, [](const P &a, const P &b) { return a[0] < b[0]; } ) );

                if(l_begin >= l_end) //There is no point that matches the range of the 1st coordinate
                    return;

                l_end--; 
                std::size_t r_begin = l_begin, r_end = l_end;

                bool below_lca = false; // Are we past the lowest common ancestor of the leaves that will be found by the searches?
                while(l<tree.size()/2 || r<tree.size()/2)
                {
                    if(l < tree.size()/2) // Is l an internal vertex?
                    {
                        if(tree[2*l+1].range_to < x[1]) // x > left_child.range_to ?
                        {
                            // Move to the right child
                            l_begin = follow_xlinking_pointer(l_begin, tree[l].right_pointers, 2*l+2);
                            l_end = follow_xlinking_pointer(l_end, tree[l].right_pointers, 2*l+2);
                            l = 2*l+2;
                        }
                        else
                        {
                            if(below_lca) // Handle the right child of the current node
                                handle_node(&tree[2*l+2], y[0], follow_xlinking_pointer(l_begin, tree[l].right_pointers, 2*l+2), follow_xlinking_pointer(l_end, tree[l].right_pointers, 2*l+2), func);


                            // Move to the left child
                            l_begin = follow_xlinking_pointer(l_begin, tree[l].left_pointers, 2*l+1);
                            l_end = follow_xlinking_pointer(l_end, tree[l].left_pointers, 2*l+1);
                            l = 2*l+1;
                        }
                    }

                    if(r < tree.size()/2) // Is r an internal vertex?
                    {
                        if(y[1] < tree[2*r+2].range_from) // y < right_child.range_from ?
                        {
                            // Move to the left child
                            r_begin = follow_xlinking_pointer(r_begin, tree[r].left_pointers, 2*r+1);
                            r_end = follow_xlinking_pointer(r_end, tree[r].left_pointers, 2*r+1);
                            r = 2*r+1;
                        }
                        else
                        {
                            if(below_lca) // Handle the left child of the current node
                                handle_node(&tree[2*r+1], y[0], follow_xlinking_pointer(r_begin, tree[r].left_pointers, 2*r+1), follow_xlinking_pointer(r_end, tree[r].left_pointers, 2*r+1), func);

                            // Move to the right child
                            r_begin = follow_xlinking_pointer(r_begin, tree[r].right_pointers, 2*r+2);
                            r_end = follow_xlinking_pointer(r_end, tree[r].right_pointers, 2*r+2);
                            r = 2*r+2;
                        }
                    }

                    below_lca = (l!=r); // If l and r are not equal then we must be below the LCA

                    if(tree[r].range_to < tree[l].range_from) // If the left and right search "cross" then there is no point of interest
                        return;
                }

                handle_node(&tree[l], y[0], l_begin, l_end, func);

                if(l!=r)
                    handle_node(&tree[r], y[0], r_begin, r_end, func);
            }


            // Returns the number of points in the rectangle defined by the points x and y
            std::size_t count_points_in_range(P x, P y)
            {
                std::size_t result = 0;
                auto counter = [&result]([[maybe_unused]] const N *node, point_iterator<P> begin, point_iterator<P> end)
                {
                    result += std::distance(begin, end);
                };

                search(x, y, counter);

                return result;
            }

            // Copies all points in the rectangle defined by x and y to the given output iterator
            template <class OutputIt> void list_points_in_range(P x, P y, OutputIt it)
            {
                auto copier = [&it]([[maybe_unused]] const N *node, point_iterator<P> begin, point_iterator<P> end)
                {
                    std::copy(begin, end, it);
                };

                search(x, y, copier);
            }

            // Calls func(p) for each point p in the rectangle defined by x and y.
            // func must expect an argument of type const P or const P&
            template <class F> void for_each_point_in_range(P x, P y, F func)
            {
                auto foreach = [&func]([[maybe_unused]] const N *node, point_iterator<P> begin, point_iterator<P> end)
                {
                    for(; begin!=end; ++begin)
                        func(*begin);
                };

                search(x, y, foreach);
            }
    };
}

#endif
