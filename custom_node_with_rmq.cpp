#include "TwoDLayeredRangeTree.h"
#include "RMQOracle.h"
#include <cstdlib>
#include <iostream>
#include <array>

using namespace range_trees;

class point {
 private:
    int coordinates[2];
    int weight;

 public:
    point(int x, int y, int weight) : coordinates {x, y}, weight(weight) {}

    point(const point &other)
    {
        *this = other;
    }
    
    point& operator=(const point &other)
    {
        coordinates[0] = other.coordinates[0];
        coordinates[1] = other.coordinates[1];
        weight = other.weight;

        return *this;
    }

    int get_weight() const { return weight; }

    int operator[](int i) const { return coordinates[i]; }

    int operator<(const point &other) const { return weight < other.weight; }
};

class custom_node
{
 private:
    RMQOracle<point_iterator<point>> *oracle = nullptr;
        
 public:
    void init(point_iterator<point> first, point_iterator<point> last)
    {
        oracle = new RMQOracle(first, last);
    }

    ~custom_node()
    {
        delete oracle;
    }

    point get_min(point_iterator<point> first, point_iterator<point> last)
    {
        return *oracle->query(first, last);
    }
};

int main()
{
    point min(0,0, 1000);
    auto update_min = [&min](custom_node *v, point_iterator<point> begin, point_iterator<point> end)
    {
        min = std::min(min, v->get_min(begin, end));
    };
    
    TwoDLayeredRangeTree<point, custom_node> range_tree = { {9, 6, 5}, {4, 8, 2}, {7, 9, 3}, {2, 5, 1} };
    range_tree.search( {1, 7, 0}, {8, 10, 0}, update_min);

    std::cout << "The point of minimum weight is (" << min[0] << ", " << min[1] << ") and has weight " << min.get_weight() << "\n";

    return EXIT_SUCCESS;
}
