#include "TwoDLayeredRangeTree.h"
#include <cstdlib>
#include <iostream>
#include <array>
#include <vector>
#include <random>
#include <chrono>

using namespace range_trees;

typedef std::array<int, 2> point;

int main()
{   
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::cout << "Using seed " << seed << "\n";

    std::default_random_engine rng(seed);
    std::uniform_int_distribution<int> dist(0, 100);

    std::vector<point> points;
    points.reserve(1000); 
    for(int i=0; i<1000; i++)
        points.push_back( { dist(rng), dist(rng) } );

    TwoDLayeredRangeTree<point> range_tree(points.begin(), points.end());

    for(int i=0; i<1000000; i++)
    {
        int r1 = dist(rng), r2 = dist(rng);
        point p1 =  { std::min(r1, r2), std::max(r1, r2) };

        r1 = dist(rng);
        r2 = dist(rng);
        point p2 = { std::min(r1, r2), std::max(r1, r2) };

        std::vector<point> output;
        range_tree.list_points_in_range(p1, p2, std::back_inserter(output));

        std::vector<point> output2;
        output2.reserve(output.size());
        std::copy_if(output.begin(), output.end(), std::back_inserter(output2), [&p1, &p2](const point p){ 
            return p1[0] <= p[0] && p[0] <= p2[0] && p1[1] <= p[1] && p[1] <= p2[1];
        });

        std::sort(output.begin(), output.end());
        std::sort(output2.begin(), output2.end());

        if( output.size() != output2.size() || output.size() != range_tree.count_points_in_range(p1, p2) || !std::equal(output.begin(), output.end(), output2.begin()) )
        {
            std::cout << "\nFail " << p1[0] << " " << p1[1] <<" " << p2[0] << " " << p2[1] << "\n";
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
