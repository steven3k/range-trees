// Copyright (c) 2022 Stefano Leucci
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __RMQORACLE_H__
#define __RMQORACLE_H__

#include<vector>
#include<bit>
#include<type_traits>

//The "Sparse Table" RMQ oracle
template<std::random_access_iterator Iter, typename Compare = std::less<std::iter_value_t<Iter>>> class RMQOracle
{
 private:
    typedef std::iter_difference_t<Iter> T;

    const Iter begin;
    std::vector<std::vector<Iter>> M; //M[i][j] contains the index of the minimum in D[i],...,D[i+2^j]
    const Compare comp; 

 public:
    RMQOracle(const Iter first, const Iter last, const Compare& comp = Compare()) : begin(first), M(last - first), comp(comp)
    {
        const T size = last-first;
            
        for(T i = 0; i < size; i++)
            M[i].push_back(first + i);
        
        T jump_size = 2;
        for(unsigned int j = 1; jump_size < size; j++, jump_size *= 2)            
        {
            for(T i=0; i < size; i++)
            {
                if(i + jump_size - 1 < size)
                    M[i].push_back( comp(*M[i][j-1], *M[i + jump_size/2][j-1]) ? M[i][j-1] : M[i + jump_size/2][j-1] );
                else
                    M[i].push_back(M[i][j-1]);
            }
        }
    }
    
    Iter query(const Iter first, const Iter last) const
    {
        const T i = first - begin;
        const T j = last - begin;

        const int l = std::bit_width(static_cast<std::make_unsigned_t<T>>(j-i)) - 1;
        const T jump_size = T(1) << l;

        return comp(*M[i][l], *M[j - jump_size][l]) ? M[i][l] : M[j - jump_size][l];
    }
};

#endif
